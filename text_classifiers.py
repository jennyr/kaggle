from custom_transformers import *
from data_loader import DataLoader
from sklearn.pipeline import FeatureUnion
from sklearn.pipeline import Pipeline
from sklearn.linear_model import SGDClassifier, LogisticRegression
from sklearn.feature_extraction.text import TfidfVectorizer
from graph_features import GraphFeatures
from sklearn.preprocessing import StandardScaler
import csv
from sklearn.decomposition import TruncatedSVD
import nltk
nltk.download('stopwords')
nltk.download('punkt')
nltk.download('wordnet')
nltk.download('averaged_perceptron_tagger')
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import CountVectorizer

def identity(arg):
	"""
	Simple identity function works as a passthrough.
	"""
	return arg


def average_word_length(name):
	"""Helper code to compute average word length of a name"""
	if (len(name) == 0):
		return 0
	return np.mean([len(word) for word in name.split()])


def number_of_authors(names):
	num_of_authors = 0
	if (len(names) > 0):
		num_of_authors = names.count(",") + 1
	return  num_of_authors
	
	
if __name__== "__main__":
	
	data = DataLoader()
	x_train = data.train_data_labels_info[["title", "abstract", "authors", "out_degree", "in_degree", "avg_neig_deg"]]
	x_test = data.test_data_labels_info[["title", "abstract",  "authors", "out_degree", "in_degree", "avg_neig_deg"]]
	y_train = data.train_data_labels_info["Journal"].tolist()
	x_train = x_train.replace(np.nan, '', regex=True)
	x_train["avg_word_len_auth"] = x_train["authors"].apply(average_word_length)
	x_train["num_auth"] = x_train["authors"].apply(number_of_authors)
	x_test = x_test.replace(np.nan, '', regex=True)
	x_test["avg_word_len_auth"] = x_test["authors"].apply(average_word_length)
	x_test["num_auth"] = x_test["authors"].apply(number_of_authors)

	

	classifier = LogisticRegression(penalty='l2', tol=0.0001)
	
	pipeline = Pipeline([
		
		('union', FeatureUnion(
			transformer_list=[
				
				('number_of_authors', Pipeline([
					('selector', NumberSelector(field='num_auth')),
					('normalizer', StandardScaler(copy=True, with_mean=True, with_std=True)),
				])),
				# Pipeline for pulling features from the post's subject line
				('title', Pipeline([
					('selector', TextSelector(field='title')),
					('preprocessor', TextPreprocessor()),
					('vectorizer', TfidfVectorizer(
						tokenizer=identity, preprocessor=None, lowercase=False, max_df=0.6, min_df=0.001, ngram_range=(1, 2)
					)),
				])),
				# Pipeline for pulling features from authors
				('author', Pipeline([
					('selector', TextSelector(field='authors')),
					('vect', CountVectorizer(decode_error='ignore', stop_words='english', max_df=0.03, min_df=0,
					                         ngram_range=(1, 2))),
					('tfidf', TfidfTransformer(norm='l2', sublinear_tf=True)),
				])),
				('avg_word_len_auth', Pipeline([
					('selector', NumberSelector(field='avg_word_len_auth')),
					('normalizer', StandardScaler(copy=True, with_mean=True, with_std=True)),
				])),
				('out_degree', Pipeline([
					('selector', NumberSelector(field='out_degree')),
					('normalizer', StandardScaler(copy=True, with_mean=True, with_std=True)),
				])),
				('in_degree', Pipeline([
					('selector', NumberSelector(field='in_degree')),
					('normalizer', StandardScaler(copy=True, with_mean=True, with_std=True)),
				])),
				('avg_neig_deg', Pipeline([
					('selector', NumberSelector(field='avg_neig_deg')),
					('normalizer', StandardScaler(copy=True, with_mean=True, with_std=True)),
				])),
				# Pipeline for standard bag-of-words model for body
				('abstract', Pipeline([
					('selector', TextSelector(field='abstract')),
					('preprocessor', TextPreprocessor()),
					('vectorizer', TfidfVectorizer(tokenizer=identity, preprocessor=None, lowercase=False, max_df=0.6, min_df=0.001, ngram_range=(1, 2))),
				])),
			],
			
			transformer_weights={
				'title': 1.65,
				'out_degree': 0.7,
				'in_degree': 0.9,
				'avg_neig_deg': 0.8,
				'abstract': 1.65,
				'author': 1.5
			},
		)),

		('classifier', classifier),
	])
	
	pipeline.fit(x_train, y_train)
	
	# Get test IDs too
	test_ids = list()
	with open('./data/test.csv', 'r') as f:
		next(f)
		for line in f:
			test_ids.append(line[:-2])
	
	y_pred = pipeline.predict_proba(x_test)
	
	# Write predictions to a file
	with open('sample_submission.csv', 'w') as csvfile:
		writer = csv.writer(csvfile, delimiter=',')
		lst = pipeline.classes_.tolist()
		lst.insert(0, "Article")
		writer.writerow(lst)
		for i, test_id in enumerate(test_ids):
			lst = y_pred[i, :].tolist()
			lst.insert(0, test_id)
			writer.writerow(lst)