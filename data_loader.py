import pandas as pd
import numpy as np
import string
import csv
import re

from graph_features import GraphFeatures
class DataLoader(object):

	DATA_NODE_PATH = "./data/node_information.csv"
	TRAIN_DATA_PATH = "./data/train.csv"
	TEST_DATA_PATH = "./data/test.csv"
	num_of_sub = 4 - 2.05335

	def __init__(self):
		graph_features = GraphFeatures()
		graph_features.read_train_data()
		graph_features.read_test_data()
		
		X_train = graph_features.create_features_matrix("train")
		X_test = graph_features.create_features_matrix("test")

		print("\nTrain matrix dimensionality: ", X_train.shape)
		print("Test matrix dimensionality: ", X_test.shape)
		X_train_df = pd.DataFrame(data=X_train, columns=['out_degree', 'in_degree', 'avg_neig_deg'])
		X_train_df['Article'] = graph_features.train_ids
		X_train_df['Article'] = X_train_df['Article'].astype('int64')
		
		X_test_df = pd.DataFrame(data=X_test, columns=['out_degree', 'in_degree', 'avg_neig_deg'])
		X_test_df['Article'] = graph_features.test_ids
		X_test_df['Article'] = X_test_df['Article'].astype('int64')
		
		self.data_node = pd.read_csv(self.DATA_NODE_PATH)
		self.train_data = pd.read_csv(self.TRAIN_DATA_PATH)
		self.test_data = pd.read_csv(self.TEST_DATA_PATH)
		self.completed_train_data = pd.merge(self.train_data, X_train_df, how='left', left_on=['Article'], right_on=['Article'])
		self.completed_test_data = pd.merge(self.test_data, X_test_df, how='left', left_on=['Article'], right_on=['Article'])
		self.train_data_labels_info = pd.merge(self.completed_train_data, self.data_node, how='left', left_on=['Article'], right_on=['id'])
		self.test_data_labels_info = pd.merge(self.completed_test_data, self.data_node, how='left', left_on=['Article'], right_on=['id'])


if __name__== "__main__":
	
	data_loader = DataLoader()
	a = 1

